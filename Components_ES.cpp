/* 
 * File:   Components_ES.cpp
 * Author: PedroLuis
 * 
 * Created on 26 de septiembre de 2011, 16:18
 */

#include "Components_ES.h"

Components_ES::Components_ES()  {
    initComponents_ES();
}

void Components_ES::initComponents_ES() {
    widgetRight_ES = new QWidget();
    group_ES = new QGroupBox[5];
    ver_ES = new QVBoxLayout(widgetRight_ES);
    grid_Escale = new QGridLayout(&group_ES[0]);
    grid_Translate = new QGridLayout(&group_ES[1]);
    grid_Rotate = new QGridLayout(&group_ES[2]);
    grid_Rotate_Eje = new QGridLayout(&group_ES[3]);
    grid_View = new QGridLayout(&group_ES[4]);
    button_ES = new QPushButton[17];
    edit_Es = new QLineEdit[6];

    /*-----------Escalar---------*/
    group_ES[0].setTitle("Escalar");
    button_ES[0].setText("+");
    button_ES[1].setText("-");

    /*---------Trasladar---------*/
    group_ES[1].setTitle("Trasladar");
    button_ES[2].setText("+");
    button_ES[3].setText("+");
    button_ES[4].setText("+");
    button_ES[5].setText("-");
    button_ES[6].setText("-");
    button_ES[7].setText("-");
    edit_Es[0].setText("1");
    edit_Es[1].setText("1");
    edit_Es[2].setText("1");

    /*----------Rotar------------*/
    group_ES[2].setTitle("Rotar");;
    button_ES[8].setText("+");
    button_ES[9].setText("+");
    button_ES[10].setText("+");
    button_ES[11].setText("-");
    button_ES[12].setText("-");
    button_ES[13].setText("-");

    /*---------Rotar eje---------*/
    group_ES[3].setTitle("Rotar Eje");
    button_ES[14].setText("Eje");
    edit_Es[3].setText("5");
    edit_Es[4].setText("-5");
    edit_Es[5].setText("5");

    /*----------Vista------------*/
    group_ES[4].setTitle("Vista");
    button_ES[15].setText("<--");
    button_ES[16].setText("-->");
    /*|||||||||||||||||||||||||||*/
    grid_Escale->addWidget(&button_ES[0], 0, 0);
    grid_Escale->addWidget(new QLabel(), 0, 1);
    grid_Escale->addWidget(&button_ES[1], 0, 2);
    
    grid_Translate->addWidget(new QLabel("x"), 0, 0);
    grid_Translate->addWidget(new QLabel("y"), 0, 1);
    grid_Translate->addWidget(new QLabel("z"), 0, 2);
    grid_Translate->addWidget(&edit_Es[0], 1, 0);
    grid_Translate->addWidget(&edit_Es[1], 1, 1);
    grid_Translate->addWidget(&edit_Es[2], 1, 2);
    grid_Translate->addWidget(&button_ES[2], 2, 0);
    grid_Translate->addWidget(&button_ES[3], 2, 1);
    grid_Translate->addWidget(&button_ES[4], 2, 2);
    grid_Translate->addWidget(&button_ES[5], 3, 0);
    grid_Translate->addWidget(&button_ES[6], 3, 1);
    grid_Translate->addWidget(&button_ES[7], 3, 2);
    
    grid_Rotate->addWidget(new QLabel("x"), 0, 0);
    grid_Rotate->addWidget(new QLabel("y"), 0, 1);
    grid_Rotate->addWidget(new QLabel("z"), 0, 2);
    grid_Rotate->addWidget(&button_ES[8], 1, 0);
    grid_Rotate->addWidget(&button_ES[9], 1, 1);
    grid_Rotate->addWidget(&button_ES[10], 1, 2);
    grid_Rotate->addWidget(&button_ES[11], 2, 0);
    grid_Rotate->addWidget(&button_ES[12], 2, 1);
    grid_Rotate->addWidget(&button_ES[13], 2, 2);
    
    grid_Rotate_Eje->addWidget(new QLabel(), 0, 0);
    grid_Rotate_Eje->addWidget(&button_ES[14], 0, 1);
    grid_Rotate_Eje->addWidget(new QLabel(), 0, 2);
    grid_Rotate_Eje->addWidget(&edit_Es[3], 1, 0);
    grid_Rotate_Eje->addWidget(&edit_Es[4], 1, 1);
    grid_Rotate_Eje->addWidget(&edit_Es[5], 1, 2);
    
    grid_View->addWidget(&button_ES[15], 0, 0);
    grid_View->addWidget(&button_ES[16], 0, 1);
    
    grid_Escale->setRowStretch(1, 10);
    grid_Escale->setColumnStretch(3, 10);
    
    grid_Translate->setRowStretch(3, 10);
    grid_Translate->setColumnStretch(3, 10);
    
    grid_Rotate->setRowStretch(3, 10);
    grid_Rotate->setColumnStretch(3, 10);
    
    grid_Rotate_Eje->setRowStretch(1, 10);
    grid_Rotate_Eje->setColumnStretch(3, 10);
    
    grid_View->setRowStretch(1, 10);
    grid_View->setColumnStretch(3, 10);
    
    group_ES[0].setLayout(grid_Escale);
    group_ES[1].setLayout(grid_Translate);
    group_ES[2].setLayout(grid_Rotate);
    group_ES[3].setLayout(grid_Rotate_Eje);
    group_ES[4].setLayout(grid_View);
    
    ver_ES->addWidget(&group_ES[0]);
    ver_ES->addWidget(&group_ES[1]);
    ver_ES->addWidget(&group_ES[2]);
    ver_ES->addWidget(&group_ES[3]);
    //ver_ES->addWidget(&group_ES[4]);
    
    widgetRight_ES->setLayout(ver_ES);
}

Components_ES::~Components_ES() {
    delete widgetRight_ES;
    delete []group_ES;
    delete []button_ES;
    delete []edit_Es;
    delete grid_Escale;
    delete grid_Translate;
    delete grid_Rotate;
    delete grid_Rotate_Eje;
    delete grid_View;
}

