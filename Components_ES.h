/* 
 * File:   Components_ES.h
 * Author: PedroLuis
 *
 * Created on 26 de septiembre de 2011, 16:18
 */

#ifndef COMPONENTS_ES_H
#define	COMPONENTS_ES_H

#include <QPushButton>
#include <QLineEdit>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QWidget>
#include <QFont>

class Components_ES {
public:
    Components_ES();
    void initComponents_ES();
    virtual ~Components_ES();

protected:
    QWidget *widgetRight_ES;
    QGroupBox *group_ES;
    QPushButton *button_ES;
    QLineEdit *edit_Es;
    QVBoxLayout *ver_ES;
    QGridLayout *grid_View;
    QGridLayout *grid_Escale;
    QGridLayout *grid_Translate;
    QGridLayout *grid_Rotate;
    QGridLayout *grid_Rotate_Eje;
};

#endif	/* COMPONENTS_ES_H */

