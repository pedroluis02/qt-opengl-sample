/* 
 * File:   Escena_GL.cpp
 * Author: PedroLuis
 * 
 * Created on 25 de septiembre de 2011, 20:16
 */

#include "Escena_GL.h"

Escena_GL::Escena_GL(QWidget *parent) : QGLWidget(parent) {
}

void Escena_GL::initializeGL() {
    glClearColor(0.f , 0.f , 0.f, 1.f);
    glShadeModel(GL_FLAT);
    
    object3dIndex = glGenLists(1);
    glNewList(object3dIndex, GL_COMPILE);
        glBegin(GL_LINES);
            glColor3f(0, 0, 1);
            glVertex3i(0, 0, 0);
            glVertex3i(0, 0, 10);
            
            glColor3f(0, 1, 0);
            glVertex3i(0, 0, 0);
            glVertex3i(0, 10, 0);
            
            glColor3f(1, 0, 0);
            glVertex3i(0, 0, 0);
            glVertex3i(10, 0, 0);
            
        glEnd();
    glEndList();
}

void Escena_GL::resizeGL(int w, int h) {
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-10, 10, -10, 10, -10, 10);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(5, 5, 5, -5, -5, -5, 0, 0, 5);
    glViewport(0, 0, w, h);
}

void Escena_GL::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glCallList(object3dIndex);
    glColor3f(1, 0, 0);
    renderText(11, 0, 0, "X", QFont("System"));
    glColor3f(0, 1, 0);
    renderText(0, 11, 0, "Y", QFont("System"));
    glColor3f(0, 0, 1);
    renderText(0, 0, 11, "Z", QFont("System"));
    glColor3f(1, 1, 1);

    cylinder.paintGraphis3D();
}

void Escena_GL::keyPressEvent(QKeyEvent* ke) {
    switch (ke->key()) {
    }
}

void Escena_GL::escale_Obj3d(double x, double y, double z) {
    double coord[] = {x, y, z};
    cylinder.setTransformation3D(new Transformation3D(coord, 
            Transformation3D::T3D_SCALATION));
    updateGL(); 
}

void Escena_GL::translate_Obj3d(double x, double y, double z) {
    double coord[] = {x, y, z};
    cylinder.setTransformation3D(new Transformation3D(coord, Transformation3D::T3D_TRANSLATION));
    updateGL(); 
}

void Escena_GL::rotateEje_Obj3d(double a, double x, double y, double z) {
    double coord[] = {x, y, z};
    cylinder.setTransformation3D(new Transformation3D(a, coord, Transformation3D::T3D_ROTATATION));
    updateGL(); 
}

Escena_GL::~Escena_GL() {
}
