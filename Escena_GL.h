/* 
 * File:   Escena_GL.h
 * Author: PedroLuis
 *
 * Created on 25 de septiembre de 2011, 20:16
 */

#ifndef ESCENA_GL_H
#define	ESCENA_GL_H

#include <QGLWidget>
#include <QtOpenGL>
#include <qgl.h>

#include <QKeyEvent>
#include <QColor>
#include <QPaintEvent>

#include <QRectF>

#include "Components_ES.h"
#include "Graphic3D.h"

class Escena_GL : public QGLWidget {
    Q_OBJECT

public:
    Escena_GL(QWidget *parent=0);
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void keyPressEvent(QKeyEvent *ke);
    virtual ~Escena_GL();
    /*-------------------------------*/
    void escale_Obj3d(double x, double y,double z);
    void translate_Obj3d(double x, double y, double z);
    void rotateEje_Obj3d(double a, double x, double y, double z);

private:
//    void cylinder_ES();
    int object3dIndex; 
    Graphic3D cylinder;
};

#endif	/* ESCENA_GL_H */

