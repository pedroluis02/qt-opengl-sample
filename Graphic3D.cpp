/* 
 * File:   Graphic3D.cpp
 * Author: Pedro Luis
 * 
 * Created on 15 de octubre de 2011, 9:39
 */

#include "Graphic3D.h"

Graphic3D::Graphic3D() {
    visible = false;
}

void Graphic3D::cylinder_ES() {
    GLUquadric *cuadric = gluNewQuadric();
    gluQuadricDrawStyle(cuadric, GLU_LINE);
    gluQuadricNormals(cuadric, GLU_FLAT);
    //gluCylinder(cuadric, 1, 1, 1, 50, 50);
    gluSphere(cuadric, 5, 50, 50);
}

void Graphic3D::setTransformation3D(Transformation3D *t3d) {
    transformations3D.push_back(t3d);
    
    if(t3d->transf_type == Transformation3D::T3D_SCALATION)
        mat.scale(t3d->coord3d[0], t3d->coord3d[1], t3d->coord3d[2]); 
    else if(t3d->transf_type == Transformation3D::T3D_TRANSLATION)
        mat.translate(t3d->coord3d[0], t3d->coord3d[1], t3d->coord3d[2]);
    else if(t3d->transf_type == Transformation3D::T3D_ROTATATION)
        mat.rotate(t3d->theta, t3d->coord3d[0], t3d->coord3d[1], 
                t3d->coord3d[2]);
}

void Graphic3D::paintGraphis3D() {
    glPushMatrix();
        glMultMatrixf(mat.constData());
        cylinder_ES();
    glPopMatrix();
}

void Graphic3D::setVisible(bool visible) {
    this->visible = visible;
}

bool Graphic3D::isVisible() {
    return visible;
}

Graphic3D::~Graphic3D() {
}

Transformation3D::Transformation3D() {
    this->theta = 0.0;
    this->coord3d = NULL;
    this->transf_type = T3D_NONE;
}

Transformation3D::Transformation3D(const double* coord3d,
        TRANSFORMATIONS_3D transf_op) {
    this->coord3d = coord3d;
    this->transf_type = transf_op;
}

Transformation3D::Transformation3D(double theta, const double* coord3d,
        TRANSFORMATIONS_3D transf_op) {
    this->theta = theta;
    this->coord3d = coord3d;
    this->transf_type = transf_op;
}

void Transformation3D::escale_Obj3d() {
    glScaled(coord3d[0], coord3d[1], coord3d[2]);
}

void Transformation3D::translate_Obj3d() {
    glTranslated(coord3d[0], coord3d[1], coord3d[2]);
}

void Transformation3D::rotateEje_Obj3d() {
    glRotated(theta, coord3d[0], coord3d[1], coord3d[2]);
}

Transformation3D::~Transformation3D() {
}
