/* 
 * File:   Graphic3D.h
 * Author: Pedro Luis
 *
 * Created on 15 de octubre de 2011, 9:39
 */

#include <QtOpenGL>

#include <QVector>
#include <QList>

#include <QMatrix4x4>

#include  <GL/glu.h>

#ifndef GRAPHIC3D_H
#define	GRAPHIC3D_H

class Transformation3D {
public:
    enum TRANSFORMATIONS_3D {
        T3D_NONE,
        T3D_SCALATION,
        T3D_TRANSLATION,
        T3D_ROTATATION        
    };
    
    Transformation3D();
    Transformation3D(const double *coord3d,
            TRANSFORMATIONS_3D transf_op);
    Transformation3D(double theta, const double *coord3d,
            TRANSFORMATIONS_3D transf_op);
    void escale_Obj3d();
    void translate_Obj3d();
    void rotateEje_Obj3d();
    
    virtual ~Transformation3D();
    
    double theta;
    const double *coord3d;
    TRANSFORMATIONS_3D transf_type;
};

class Graphic3D  {
public:
    Graphic3D();
    void cylinder_ES();
    void setTransformation3D(Transformation3D *t3d);
    void paintGraphis3D();
    void setVisible(bool visible);
    bool isVisible();
    virtual ~Graphic3D();

private:
    QList <Transformation3D*> transformations3D;
    QMatrix4x4 mat;
    bool visible;
};

#endif	/* GRAPHIC3D_H */

