/* 
 * File:   Main_EScena.cpp
 * Author: PedroLuis
 * 
 * Created on 27 de septiembre de 2011, 20:05
 */

#include "Main_EScena.h"

Main_EScena::Main_EScena(QWidget *parent) : QMainWindow(parent), Components_ES() {
    centralWidget_ES = new QWidget(this);
    hor_ES = new QHBoxLayout(centralWidget_ES);
    escena = new Escena_GL(centralWidget_ES);
    widgetRight_ES->setParent(centralWidget_ES);
    hor_ES->addWidget(widgetRight_ES, Qt::AlignLeft);
    hor_ES->addWidget(escena, Qt::AlignCenter);
    centralWidget_ES->setLayout(hor_ES);
    setWindowTitle("Qt/OpenGL - Transformaciones");
    setGeometry(100, 50, 800, 500);
    setMinimumSize(800, 500);
    setCentralWidget(centralWidget_ES);
    initEvents_ES();
}

void Main_EScena::initEvents_ES() {
    connect(&button_ES[0], SIGNAL(clicked()), this, SLOT(slot_EscalePx()));
    connect(&button_ES[1], SIGNAL(clicked()), this, SLOT(slot_EscaleMx()));

    /*-------------------------------------------------------------------*/
    connect(&button_ES[2], SIGNAL(clicked()), this, SLOT(slot_TransPx()));
    connect(&button_ES[3], SIGNAL(clicked()), this, SLOT(slot_TransPy()));
    connect(&button_ES[4], SIGNAL(clicked()), this, SLOT(slot_TransPz()));
    connect(&button_ES[5], SIGNAL(clicked()), this, SLOT(slot_TransMx()));
    connect(&button_ES[6], SIGNAL(clicked()), this, SLOT(slot_TransMy()));
    connect(&button_ES[7], SIGNAL(clicked()), this, SLOT(slot_TransMz()));

    /*------------------------------------------------------------------*/
    connect(&button_ES[8], SIGNAL(clicked()), this, SLOT(slot_RotatePx()));
    connect(&button_ES[9], SIGNAL(clicked()), this, SLOT(slot_RotatePy()));
    connect(&button_ES[10], SIGNAL(clicked()), this, SLOT(slot_RotatePz()));
    connect(&button_ES[11], SIGNAL(clicked()), this, SLOT(slot_RotateMx()));
    connect(&button_ES[12], SIGNAL(clicked()), this, SLOT(slot_RotateMy()));
    connect(&button_ES[13], SIGNAL(clicked()), this, SLOT(slot_RotateMz()));

    /*--------------------------------------------------------------------*/
    connect(&button_ES[14], SIGNAL(clicked()), this, SLOT(slot_RotateEje()));
}

void Main_EScena::transformation_Obj3d(TRANSFORMATIONS_ES trans_op) {
    double a = 30.0, x, y, z;
    switch(trans_op) {
        case ESCALE_PX:
            escena->escale_Obj3d(1.2, 1.2, 1.2);
            break;
        case ESCALE_MX:
            escena->escale_Obj3d(0.9, 0.9, 0.9);
            break;
        case TRANSL_PX:
            x = edit_Es[0].text().toDouble();
            escena->translate_Obj3d(x, 0, 0);
            break;
        case TRANSL_MX:
            x = edit_Es[0].text().toDouble();
            escena->translate_Obj3d(-x, 0, 0);
            break;
        case TRANSL_PY:
            y = edit_Es[1].text().toDouble();
            escena->translate_Obj3d(0, y, 0);
            break;
        case TRANSL_MY:
            y = edit_Es[1].text().toDouble();
            escena->translate_Obj3d(0, -y, 0);
            break;
        case TRANSL_PZ:
            z = edit_Es[2].text().toDouble();
            escena->translate_Obj3d(0, 0, z);
            break;
        case TRANSL_MZ:
            z = edit_Es[2].text().toDouble();
            escena->translate_Obj3d(0, 0, -z);
            break;
        case ROTATE_PX:
            escena->rotateEje_Obj3d(a, 1, 0, 0);
            break;
        case ROTATE_MX:
            escena->rotateEje_Obj3d(-a, 1, 0, 0);
            break;
        case ROTATE_PY:
            escena->rotateEje_Obj3d(a, 0, 1, 0);
            break;
        case ROTATE_MY:
            escena->rotateEje_Obj3d(-a, 0, 1, 0);
            break;
        case ROTATE_PZ:
            escena->rotateEje_Obj3d(a, 0, 0, 1);
            break;
        case ROTATE_MZ:
            escena->rotateEje_Obj3d(-a, 0, 0, 1);
            break;
        case ROTATE_EJE:
            x = edit_Es[3].text().toDouble();
            y = edit_Es[4].text().toDouble();
            z = edit_Es[5].text().toDouble();
            escena->rotateEje_Obj3d(a, x, y, z);
            break;
        case TRANS_NONE:
            break;
    }
}

void Main_EScena::slot_EscalePx() {
    transformation_Obj3d(ESCALE_PX);
}

void Main_EScena::slot_EscaleMx() {
    transformation_Obj3d(ESCALE_MX);
}

/*-----------------------------------*/
void Main_EScena::slot_TransPx() {
    transformation_Obj3d(TRANSL_PX);
}

void Main_EScena::slot_TransMx() {
    transformation_Obj3d(TRANSL_MX);
}

void Main_EScena::slot_TransPy() {
    transformation_Obj3d(TRANSL_PY);
}

void Main_EScena::slot_TransMy() {
    transformation_Obj3d(TRANSL_MY);
}

void Main_EScena::slot_TransPz() {
    transformation_Obj3d(TRANSL_PZ);
}

void Main_EScena::slot_TransMz() {
    transformation_Obj3d(TRANSL_MZ);
}
/*---------------------------------------*/
void Main_EScena::slot_RotatePx() {
    transformation_Obj3d(ROTATE_PX);
}

void Main_EScena::slot_RotateMx() {
    transformation_Obj3d(ROTATE_MX);
}

void Main_EScena::slot_RotatePy() {
    transformation_Obj3d(ROTATE_PY);
}

void Main_EScena::slot_RotateMy() {
    transformation_Obj3d(ROTATE_MY);
}

void Main_EScena::slot_RotatePz() {
    transformation_Obj3d(ROTATE_PZ);
}

void Main_EScena::slot_RotateMz() {
    transformation_Obj3d(ROTATE_MZ);
}

/*----------------------------------------*/
void Main_EScena::slot_RotateEje() {
    transformation_Obj3d(ROTATE_EJE);
}

/*-----------------------------------------*/
void Main_EScena::closeEvent(QCloseEvent*) {
    exit(0);
}

Main_EScena::~Main_EScena()  {
    delete centralWidget_ES;
    delete escena;
    delete hor_ES;
}

