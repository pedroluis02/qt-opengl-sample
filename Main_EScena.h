/* 
 * File:   Main_EScena.h
 * Author: PedroLuis
 *
 * Created on 27 de septiembre de 2011, 20:05
 */

#ifndef MAIN_ESCENA_H
#define	MAIN_ESCENA_H

#include <QMainWindow>
#include <QCloseEvent>

#include <QDebug>

#include "Escena_GL.h"

class Main_EScena : public QMainWindow, Components_ES {
    Q_OBJECT;
public:
    
    enum TRANSFORMATIONS_ES {
        TRANS_NONE,
        ESCALE_PX, ESCALE_MX , //ESCALE_PY, ESCALE_MY, ESCALE_PZ, ESCALE_MZ, 
        TRANSL_PX, TRANSL_MX, TRANSL_PY, TRANSL_MY, TRANSL_PZ, TRANSL_MZ,
        ROTATE_PX, ROTATE_MX, ROTATE_PY, ROTATE_MY, ROTATE_PZ, ROTATE_MZ,
        ROTATE_EJE
    };
    
    Main_EScena(QWidget *parent=0);
    void closeEvent(QCloseEvent *ce);
    virtual ~Main_EScena();

    void initEvents_ES();
    void transformation_Obj3d(TRANSFORMATIONS_ES trans_op);
    
    QWidget *centralWidget_ES;
    QHBoxLayout *hor_ES;
    Escena_GL *escena;
    
private slots:
    void slot_EscalePx();
    void slot_EscaleMx();

    void slot_TransPx();
    void slot_TransMx();
    void slot_TransPy();
    void slot_TransMy();
    void slot_TransPz();
    void slot_TransMz();

    void slot_RotatePx();
    void slot_RotateMx();
    void slot_RotatePy();
    void slot_RotateMy();
    void slot_RotatePz();
    void slot_RotateMz();

    void slot_RotateEje();
};

#endif	/* MAIN_ESCENA_H */

