/*
 * File:   main.cpp
 * Author: PedroLuis
 *
 * Created on 25 de septiembre de 2011, 20:14
 */

#include <QApplication>
#include "Main_EScena.h"

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);
    QApplication::setColorSpec( QApplication::CustomColor);
    QApplication app(argc, argv);
    if (!QGLFormat::hasOpenGL()) {
	qWarning("This system has no OpenGL support....Exiting.");
	return -1;
    }
    // create and show your widgets here
    
    Main_EScena escena;
    escena.show();
    
    return app.exec();
}

